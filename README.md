# Palopoli - Marchetti 2020

This repository includes R scripts and data files related with our upcoming manuscript *Intrinsically disordered protein ensembles shape evolutionary rates revealing conformational patterns*.

In the **src** folder there is an RMarkdown file **Palopoli-Marchetti-2020.Rmd** which can be run to reproduce all figures in the manuscript from the attached data in the **data** folder. The file **data/data_description.md** describes the contents of the data files.

The HTML file **src/Palopoli-Marchetti-2020.html** allows for easier visualization of the code and figures mentioned above.  


