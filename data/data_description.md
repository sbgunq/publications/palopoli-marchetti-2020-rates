# Brief description of the data tables used and its columns

## fig1_dis_final_gus_20_4_visual.csv
- **dataset**: refers to whether this data is from disorder proteins or not.
- **pdb_chain**: PDB_ID code for each protein in the data set
- **model**: NMR model number of each conformation
- **position**: sequential position of each model in each NMR ensemble
- **is_disorder / is_disorer_es**: **1** if that position is predicted to be disordered (see methods) and **0** if it’s not. 
- **check_dis**: column to check if the info in both columns (is_disorder / is_disorer_es) is exactly the same
- **r4sB/r4s_bayes_normavg**: evolutionary rate obtained by Bayes method, normalized by the average of the evolutionary rate in the entire data set (not used)
- **contacts**: number of contacts for each position in each NMR model
- **r4s_ml_normavg**: evolutionary rate obtained by maximum likelihood method, normalized by the average of the evolutionary rate in the entire data set (not used)
- **posname**: PDB_ID followed by position number
- **raw**: evolutionary rate obtained by Bayes method.
- **normtot**: evolutionary rate normalized by the average of the evolutionary rate of the entire dataset
- **normperprot**:  evolutionary rate normalized by the average of the evolutionary rate of each protein
- **Frac gaps/tot**: number of columns with gap in the alignment 
- **visual_check**: **1** (positive) **0** (negative), this factor was assigned after a manual and visual curation of the data set

## all_rmsf.csv
- **pdb_chain**: PDB_ID code for each protein in the data set
- **residue_number**: number of the residue in the NMR structure
- **residue_name**: name of the residue in the NMR structure
- **rmsf**: rmsf for each position 

## corr_data.csv
- **pdb_chain**: PDB_ID code for each protein in the data set
- **best_combination**: defines whether the best correlation with the evolutionary rate is obtained with the fraction of contacts (or robustness (corrfrac)) or with contact average (corrave)
- **crob**: correlation value between evolutionary rate and contact robustness (also refered as contact fraction)
- **cave**: correlation value between evolutionary rate and contact average
- **conffrac**: number of different conformation needed to obtain the best correlation between evolutionary rate and contact robustness  (also refered as contact fraction)
- **confave**: number of different conformation needed to obtain the best correlation between evolutionary rate and contact average
- **rsmd_max**: maximum value of RMSD in the whole ensemble
- **rsmd_min**: minimum value of RMSD in the whole ensemble
- **rsmd_mean**: mean value of RMSD in the whole ensemble
- **rsmd_median**: median value of RMSD in the whole ensemble

## distance_from_order.csv
- **pdb_chain**: PDB_ID code for each protein in the data set
- **position**: sequential position of each model in each NMR ensemble
- **distance_from_order**: minimus distance of each position disordered position to the nearest order position

## matriz_parseada.csv
- **pdb_chain**: PDB_ID code for each protein in the data set
- **model**: NMR model number of each conformation
- **position**: sequential position of each model in each NMR ensemble
- **second_position**: position from each model in NMR ensemble with which **position** is making contact
- **is_contact**: 1 if **position** and **second_position** established contact and 0 if they don't 

## Palopoli-Marchetti-Supplementary-Table.csv

- **PDBID**: PDB_ID code for each protein in the data set
- **ChainID**: Chain_ID for each protein in the data set
- **legnth**: lenght of each pdb structure of the proteins in the data set
- **perc_disorder**: percentage of disorder for each protein in the data set
- **num_ali_seqs**: number of sequences included in the alignment used for the calculation of evolutionary rates
